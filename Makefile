#
# This Makefile handles the bootstrapping of the Aardvark infrastructure for the Mentor API
#
# Run, *=test/prod:
# make bootstrap-*-cluster
#

.ONESHELL:

# The project name in OpenShift
PROJECT = jobtech-api-test-client

# Cluster URL's
TESTCLUSTER = "https://api.test.services.jtech.se:6443"
PRODCLUSTER = "https://api.prod.services.jtech.se:6443"

#
# Helper targets to login to OpenShift
#

.PHONY: login-test
login-test:
	@oc login $(TESTCLUSTER) --password='' --username=''

.PHONY: login-prod
login-prod:
	@echo "\033[1;31m Warning: this will upload changes to the Prod cluster \033[0m"
	@oc login $(PRODCLUSTER) --password='' --username=''

#
# Bootstrap helpers
#

.PHONY: gitcheck
gitcheck:
	@if [ ! -z "$$(git status --porcelain)" ]; then
		echo "**** error: repo is not clean"
		exit 1
	fi
	@if [ "$$(LANG=C git rev-parse --symbolic-full-name --abbrev-ref HEAD)" != "main" ]; then
		echo "**** error: not on the main branch"
		exit 1
	fi
	@git pull

#
# Bootstrap
#

.PHONY: bootstrap
bootstrap:
	@if [ -z "$(PROJECT_ENV)" ]; then
		echo "**** error: env variable PROJECT_ENV not set"
		exit 1
	else
		oc new-project $(PROJECT)-$(PROJECT_ENV)
		# Create role-bindings to Aardvark deployer
		kubectl apply -f https://gitlab.com/arbetsformedlingen/devops/aardvark/-/raw/main/docs/aardvark-deploy-bot-access-to-project.yaml
		# Crete SeviceAccounts, Services, Deployments and Routes
		kubectl apply -k kustomize/overlays/$(PROJECT_ENV)
	fi

.PHONY: bootstrap-test-cluster
bootstrap-test-cluster: gitcheck check-secrets login-test
	## Setup the Develop environment in the Test cluster
	@PROJECT_ENV=develop \
	$(MAKE) bootstrap
	## Setup the Test environment in the Test cluster
	@PROJECT_ENV=test \
	$(MAKE) bootstrap

.PHONY: bootstrap-prod-cluster
bootstrap-prod-cluster: gitcheck check-secrets login-prod
	## Setup the Develop environment in the Prod cluster
	@PROJECT_ENV=prod \
	$(MAKE) bootstrap
